# About

1. Pluginless
2. Replaces all four rope bridge models with higher resolution ones (still use vanilla textures) 
3. Every bridge has a swaying animation, this is purely visual and does not effect collision and 
   player while on the bridge

# Installation

For manual installation, just copy the meshes folder into your Morrowind 'Data Files'.
If using a mod manager just install this mod via it's archive (zip). 

# Credits

Feel free to redistribute, resuse, modify, and adapt the assets in this mod however you like. 
Credit is appreciated but not required. 
